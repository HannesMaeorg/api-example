﻿using Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BL
{
    public class VehicleDTO : BaseDomain
    {
        public int VehicleId { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string Model { get; set; }
        public string Mark { get; set; }
        [MaxLength(32)]
        public string NumberPlate { get; set; }

        public int PersonId { get; set; }

        public static VehicleDTO CreateFromDomain(Vehicle ct)
        {
            return new VehicleDTO()
            {
                VehicleId = ct.VehicleId,
                ReleaseDate = ct.ReleaseDate,
                Model = ct.Model,
                Mark = ct.Mark,
                NumberPlate = ct.NumberPlate,
                PersonId = ct.PersonId,
                Created = ct.Created,
                Modified = ct.Modified
            };
        }
    }
}






