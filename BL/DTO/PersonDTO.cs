﻿using Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace BL
{
    public class PersonDTO : BaseDomain
    {
        public int PersonId { get; set; }
        [MaxLength(64)]
        public string Firstname { get; set; }
        [MaxLength(64)]
        public string Lastname { get; set; }
        public string PersonalCode { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Age { get; set; }
        public virtual List<VehicleDTO> Vehicles { get; set; } = new List<VehicleDTO>();
        
        public static PersonDTO CreateFromDomain(Person p)
        {
            if (p == null) return null;

            return new PersonDTO()
            {
                PersonId = p.PersonId,
                Firstname = p.FirstName,
                Lastname = p.LastName,
                PersonalCode = p.PersonalCode,
                DateOfBirth = p.DateOfBirth,
                Age = p.Age
            };
        }

        public static PersonDTO CreateFromDomainWithContacts(Person p)
        {
            var person = CreateFromDomain(p);
            if (person == null) return null;

            person.Vehicles = p?.Vehicles?
                .Select(c => VehicleDTO.CreateFromDomain(c)).ToList();
            return person;
          
        }
    }
}