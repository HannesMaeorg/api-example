﻿using Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace BL.IFactories
{
    public interface IVehicleFactory
    {
        VehicleDTO Transform(Vehicle p);
        Vehicle Transform(VehicleDTO dto);
    }
}

