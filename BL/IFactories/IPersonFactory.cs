﻿using Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace BL.IFactories
{
    public interface IPersonFactory
    {
        PersonDTO Transform(Person p);
        Person Transform(PersonDTO dto);
        PersonDTO TransformWithObjects(Person p);
    }
}
