﻿using BL.IFactories;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Factories
{
    public class VehicleFactory : IVehicleFactory
    {
        public VehicleDTO Transform(Vehicle v)
        {
            return new VehicleDTO
            {
                VehicleId = v.VehicleId,
                ReleaseDate = v.ReleaseDate,
                Model = v.Model,
                Mark = v.Mark,
                NumberPlate = v.NumberPlate,
                Created = v.Created,
                Modified = v.Modified,
                PersonId = v.PersonId

            };
        }

        public Vehicle Transform(VehicleDTO dto)
        {
            return new Vehicle
            {
                ReleaseDate = dto.ReleaseDate,
                Model = dto.Model.ToUpper(),
                Mark = dto.Mark.ToUpper(),
                NumberPlate = dto.NumberPlate.ToUpper(),
                Created = dto.Created,
                Modified = dto.Modified,
                PersonId = dto.PersonId

            };
        }
    }
}
