﻿using BL.IFactories;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL.Factories
{
    public class PersonFactory : IPersonFactory
    {
        private readonly IVehicleFactory _vehicleFactory;

        public PersonFactory(IVehicleFactory vehicleFactory)
        {
            _vehicleFactory = vehicleFactory;
        }

        public PersonDTO Transform(Person p)
        {
            return new PersonDTO
            {
                PersonId = p.PersonId,
                Firstname = p.FirstName,
                Lastname = p.LastName,
                PersonalCode = p.PersonalCode,
                DateOfBirth = p.DateOfBirth,
                Age = p.Age,
                Created = p.Created,
                Modified = p.Modified
            };
        }

        public Person Transform(PersonDTO dto)
        {
            return new Person()
            {
                PersonId = dto.PersonId,
                FirstName = dto.Firstname,
                LastName = dto.Lastname,
                PersonalCode = dto.PersonalCode,
                DateOfBirth = dto.DateOfBirth,
                Age = dto.Age,
                Created = dto.Created,
                Modified = dto.Modified
                
            };
        }

        public PersonDTO TransformWithObjects(Person p)
        {
            var dto = Transform(p);
            if (dto == null) return null;

            dto.Vehicles = p?.Vehicles
                .Select(c => _vehicleFactory.Transform(c)).ToList();
            return dto;
        }
    }
}
