﻿using BL.Factories;
using BL.IFactories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL.Services
{
    public class PersonService : IPersonService
    {
        private readonly DAL.App.Interfaces.IAppUnitOfWork _uow;
        private readonly IPersonFactory _personFactory;
     

        public PersonService(DAL.App.Interfaces.IAppUnitOfWork uow, IPersonFactory personFactory)
        {
            _uow = uow;
            _personFactory = personFactory;
        }

        public PersonDTO AddNewPerson(PersonDTO dto)
        {
            if (FindByPersonalCode(dto.PersonalCode) != null)
            {
                return null;
            }

            var person = _personFactory.Transform(dto);
            person.Created = DateTime.Now;
            person.Modified = DateTime.Now;
            _uow.People.Add(person);
            _uow.SaveChanges();
            return _personFactory.TransformWithObjects(person);
        }

        public bool DeletePerson(int id)
        {
            var person = _uow.People.Find(id);
            if (person == null)
            {
                return false;
            }
            _uow.People.Detach(person);

            person.PersonId = id;
            _uow.People.Remove(person);
            _uow.SaveChanges();
            return true;

        }

        public List<PersonDTO> FindByName(string name)
        {
            var result = _uow.People
                .Get(filter: p => p.FirstName.ToUpper().Contains(name.ToUpper()) || p.LastName.ToUpper().Contains(name.ToUpper()))
                .Select(p => _personFactory.TransformWithObjects(p))
                .ToList();
            if (result == null)
            {
                return null;
            }
            return result;
        }

        public PersonDTO FindByPersonalCode(string code)
        {
            var result = _uow.People.Get(filter: p => p.PersonalCode == code).FirstOrDefault();
            if (result == null)
            {
                return null;
            }
            return _personFactory.TransformWithObjects(result);
        }

        public List<PersonDTO> GetAllPersons()
        {

            return _uow.People.Get(filter: p => p.IsVisible == true)
                .Select(p => _personFactory.TransformWithObjects(p))
                .ToList();
        }

        public PersonDTO GetPersonById(int personId)
        {
            var person = _uow.People.Find(personId);
            if (person == null)
            {
                return null;

            }
            return _personFactory.TransformWithObjects(person);
        }

        public PersonDTO MarkHidden(int id)
        {
            var person = _uow.People.Find(id);
            if (person == null)
            {
                return null;
            }
            _uow.People.Detach(person);

            person.PersonId = id;
            person.Modified = DateTime.Now;
            person.IsVisible = false;
            _uow.People.Update(person);
            _uow.SaveChanges();
            return _personFactory.TransformWithObjects(person);


        }

        public PersonDTO UpdatePerson(int id, PersonDTO dto)
        {
            var exists = _uow.People.Find(id);
            if (exists == null)
            {
                return null;
            }
            _uow.People.Detach(exists);

            var person = _personFactory.Transform(dto);
            person.PersonId = id;
            person.Created = exists.Created;
            person.Modified = DateTime.Now;
            _uow.People.Update(person);
            _uow.SaveChanges();
            return _personFactory.TransformWithObjects(person);
        }
    }
}
