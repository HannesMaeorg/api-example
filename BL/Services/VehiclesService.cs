﻿using BL.IFactories;
using BL.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services
{
    public class VehiclesService : IVehiclesService
    {
        private readonly DAL.App.Interfaces.IAppUnitOfWork _uow;
        private readonly IVehicleFactory _vehicleFactory;


        public VehiclesService(DAL.App.Interfaces.IAppUnitOfWork uow, IVehicleFactory vehicleFactory)
        {
            _uow = uow;
            _vehicleFactory = vehicleFactory;
        }

        public VehicleDTO AddVehicle(VehicleDTO dto)
        {
            if (FindByNumberPlate(dto.NumberPlate) != null)
            {
                return null;
            }

            var vehicle = _vehicleFactory.Transform(dto);
            vehicle.Created = DateTime.Now;
            vehicle.Modified = DateTime.Now;
            _uow.Vehicles.Add(vehicle);
            _uow.SaveChanges();
            return _vehicleFactory.Transform(vehicle);
        }

        public List<VehicleDTO> GetAll()
        {
            return _uow.Vehicles.All().Select(p => _vehicleFactory.Transform(p)).ToList();
        }

        public VehicleDTO FindByNumberPlate(String numb)
        {
            var result = _uow.Vehicles.Get(filter: p => p.NumberPlate == numb).FirstOrDefault();
            if(result == null)
            {
                return null;
            }
            return _vehicleFactory.Transform(result);
        }

        public async Task<VehicleDTO> GetById(int id)
        {
            var vehicle =  await _uow.Vehicles.FindAsync(id);
            if (vehicle == null)
            {
                return null;
            }
            return _vehicleFactory.Transform(vehicle);
        }

        public VehicleDTO UpdateVehicle(int id, VehicleDTO dto)
        {
            var exists =  _uow.Vehicles.Find(id);
            if (exists == null)
            {
                return null;
            }
            _uow.Vehicles.Detach(exists);

            var vehicle = _vehicleFactory.Transform(dto);
            vehicle.VehicleId = id;
            vehicle.Created = exists.Created;
            vehicle.Modified = DateTime.Now;
            _uow.Vehicles.Update(vehicle);
            _uow.SaveChanges();
            return _vehicleFactory.Transform(vehicle);

        }

        public bool DeleteVehicle(int id)
        {
            var vehicle = _uow.Vehicles.Find(id);
            if (vehicle == null)
            {
                return false;
            }
            _uow.Vehicles.Detach(vehicle);
            vehicle.VehicleId = id;
            _uow.Vehicles.Remove(vehicle);
            _uow.SaveChanges();
            return true;
        }

        public List<VehicleDTO> GetByPersonId(int id)
        {
            return _uow.Vehicles.Get(filter: p => p.PersonId == id).Select(p => _vehicleFactory.Transform(p)).ToList();

        }
    }
}
