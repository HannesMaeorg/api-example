﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Vehicle : BaseDomain
    {
        public int VehicleId { get; set; }
        public DateTime ReleaseDate { get; set; }
        [MaxLength(32)]
        public string Model { get; set; }
        [MaxLength(32)]
        public string Mark { get; set; }
        [MaxLength(32)]
        public string NumberPlate { get; set; }
        
        public int PersonId { get; set; }
        public virtual Person Person { get; set; }
    }
}
