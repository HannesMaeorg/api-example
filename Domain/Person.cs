﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Person : BaseDomain
    {
        public int PersonId { get; set; }
        [MaxLength(64)]
        public string FirstName { get; set; }
        [MaxLength(64)]
        public string LastName { get; set; }
        public string PersonalCode { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Age { get; set; }
        public virtual List<Vehicle> Vehicles { get; set; } = new List<Vehicle>();
        public bool IsVisible { get; set; } = true;
    }
}
