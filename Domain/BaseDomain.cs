﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class BaseDomain
    {
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
